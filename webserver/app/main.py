from flask import Flask, request, send_from_directory, send_file
from io import BytesIO
import os
import re
from PIL import Image
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

DATA_ROOT = "/opt/project/trainBlended/"


def serve_pil_image(pil_img):
    img_io = BytesIO()
    pil_img.save(img_io, 'png')
    img_io.seek(0)
    return send_file(img_io, mimetype='image/png')

def get_filenames_material_folder(material1, material2):
    path = "{}X{}".format(material1, material2)
    img_folder = os.path.join(DATA_ROOT, path)
    folder_files = os.listdir(img_folder)
    return img_folder, folder_files


@app.route('/<material1>/<material2>/<img_number>/<img_type>')
def send_static_folder(material1, material2, img_number, img_type):
    try:
        img_folder, folder_files = get_filenames_material_folder(material1, material2)
    except FileNotFoundError:
        print("\ntrying reverse order!\n\n\n")
        img_folder, folder_files = get_filenames_material_folder(material2, material1)

    if len(folder_files) == 0:
        print("fail!\n")
        return

    img_filename = os.path.join(img_folder, folder_files[int(img_number)])
    img = Image.open(img_filename)
    w = img.height
    h = img.height
    if img_type == 'roughness':
        crop_coord = (3 * w, 0, 4 * w, h)
    elif img_type == 'normal':
        crop_coord = (1 * w, 0, 2 * w, h)
    elif img_type == 'diffuse_albedo':
        crop_coord = (2 * w, 0, 3 * w, h)
    elif img_type == 'specular_albedo':
        crop_coord = (4 * w, 0, 5 * w, h)
    else:
        # error
        return
    img = img.crop(box=crop_coord)
    return serve_pil_image(img)


if __name__ == '__main__':
    app.run(debug=True)
