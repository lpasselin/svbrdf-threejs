"use strict";

window.addEventListener('resize', onWindowResize, false);

var time_clock = new THREE.Clock();
var time_delta = 0;

var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
var renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);

document.body.appendChild(renderer.domElement);
var controls = new THREE.OrbitControls(camera, renderer.domElement);

camera.position.z = 5;

// UI params control
var param = {
    light_x: 0.1, light_y: 0.1, light_z: 5, light_intensity: 1,
    bounce: false,
    material1: "ceramic_004", material2: "Chainmail", img_number: 0,
    D: "D_GGX", G: "G_GGX", F: "F_SCHLICK",
    Dataset_paper: "https://team.inria.fr/graphdeco/projects/deep-materials/"
};
var gui = new dat.GUI();
gui.add(param, 'Dataset_paper');
gui.add(param, 'light_x', -5, 5);
gui.add(param, 'light_y', -5, 5);
gui.add(param, 'light_z', 0, 20);
gui.add(param, 'light_intensity', 0, 10);
gui.add(param, 'bounce');
gui.add(param, 'material1', dataset_materials);
gui.add(param, 'material2', dataset_materials);
gui.add(param, 'img_number', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
gui.add(param, 'D', D_functions);
gui.add(param, 'G', G_functions);
gui.add(param, 'F', F_functions);
gui.open();

// light
var light = new THREE.PointLight(0xffffff);
scene.add(light);

var sphereSize = 1;
var pointLightHelper = new THREE.PointLightHelper(light, sphereSize);
scene.add(pointLightHelper);

var textureLoader = new THREE.TextureLoader();

THREE.ShaderLib.phong = { // modified version of phong
    uniforms: THREE.UniformsUtils.merge([THREE.ShaderLib.phong.uniforms, THREE.UniformsLib.roughnessmap,]),
    vertexShader: THREE.ShaderLib.phong.vertexShader,
    fragmentShader: document.getElementById('fragment').textContent,
}

var material_physically_based = new_physically_based_material();


var geometry_cube = new THREE.BoxGeometry(2, 2, 1);
geometry_cube.dynamic = true;
var material_cube = new THREE.MeshStandardMaterial({color: 0x888888});

var geometry_sphere = new THREE.SphereGeometry(0.5, 100, 100);
//var material_sphere = new THREE.MeshStandardMaterial({color: 0x888888});
var sphere = new THREE.Mesh(geometry_sphere, material_physically_based);
sphere.position.x = 2;
sphere.position.y = 1;
sphere.position.z = 0.5;
scene.add(sphere);

var cube_custom = new THREE.Mesh(geometry_cube, material_physically_based);
cube_custom.dynamic = true;
cube_custom.position.z = 0.5;
scene.add(cube_custom);

var normal_material = new THREE.MeshNormalMaterial();
var sphere_normal = new THREE.Mesh(geometry_sphere, normal_material);
sphere_normal.position.x = cube_custom.position.x + 2;
sphere_normal.position.y = cube_custom.position.y - 2;
sphere_normal.position.z = 0.5;
scene.add(sphere_normal);


var geometry_ground = new THREE.PlaneGeometry(10, 10, 10);
var ground = new THREE.Mesh(geometry_ground, normal_material);
ground.position.z = -0.01; // won't cover axesHelper
scene.add(ground);

var axesHelper = new THREE.AxesHelper(200);
scene.add(axesHelper);


var prev_param = get_current_param();

function animate() {
    requestAnimationFrame(animate);
    time_delta += time_clock.getDelta();
    if (time_delta > 1 / 60) {
        // cube.rotation.x += 0.002;
        // cube.rotation.y += 0.08;
        light.position.x = param.light_x;
        light.position.y = param.light_y;
        light.position.z = param.light_z;
        light.intensity = param.light_intensity;

        if (param_changed()) {
            prev_param = get_current_param();

            material_physically_based = new_physically_based_material();
            cube_custom.material = material_physically_based;
            sphere.material = material_physically_based;
        }


        if (param.bounce === true) sphere.position.z = sphere.position.z + Math.cos(Date.now() * 0.002) / 100;
        else sphere.position.z = 0.5;
        renderer.render(scene, camera);
        controls.update();
    }
}

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}

function get_current_param() {
    return [param.material1, param.material2, param.img_number, param.D, param.G, param.F];
}

function param_changed() {
    return (prev_param[0] != param.material1) || (prev_param[1] != param.material2) || (prev_param[2] != param.img_number)
        || (prev_param[3] != param.D) || (prev_param[4] != param.G) || (prev_param[5] != param.F);
}

function new_physically_based_material() {
    // uses gui config
    // toon uses PhongMaterial. I reuse Toon's gradientMap as my roughness map
    var m = new THREE.MeshToonMaterial({
        defines: {
            'BRDF_D_FUNCTION': param.D,
            'BRDF_G_FUNCTION': param.G,
            'BRDF_F_FUNCTION': param.F,
        }
    });

    var material_url = "http://t0ld.com/brdf-render-dataset/" + param.material1 + "/" + param.material2 + "/" + param.img_number.toString() + "/";
    m.map = textureLoader.load(material_url + "diffuse_albedo");
    m.normalMap = textureLoader.load(material_url + "normal");
    m.specularMap = textureLoader.load(material_url + "specular_albedo");
    m.gradientMap = textureLoader.load(material_url + "roughness");

    m.normalScale = new THREE.Vector2(1, 1);
    m.offset = new THREE.Vector2(0, 0.5);
    m.physicallyCorrectLights = true;

    return m;
}

animate();



